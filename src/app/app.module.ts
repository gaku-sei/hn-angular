import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ContentComponent } from './content/content.component';
import { ListStoriesComponent } from './list-stories/list-stories.component';
import { StoryService } from './story.service';
import { CardComponent } from './card/card.component';
import { FromNowPipe } from './from-now.pipe';
import { ButtonComponent } from './button/button.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContentComponent,
    ListStoriesComponent,
    CardComponent,
    FromNowPipe,
    ButtonComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    HttpModule,
  ],
  providers: [
    StoryService,
  ],
  bootstrap: [
    AppComponent,
  ],
})
export class AppModule { }
