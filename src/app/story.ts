// The server response
export interface Item {
  by: string;
  descendants: number;
  id: number;
  score: number;
  time: number;
  title: string;
  url: string;
}

export interface Story {
  author: string;
  commentCount: number;
  id: number;
  score: number;
  time: number;
  title: string;
  url: string;
}
