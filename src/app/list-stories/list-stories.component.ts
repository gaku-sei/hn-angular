import { Observable } from 'rxjs/Rx';
import { Component, OnInit } from '@angular/core';

import { StoryService } from '../story.service';
import { Story } from '../story';

@Component({
  selector: 'app-list-stories',
  templateUrl: './list-stories.component.html',
  styleUrls: ['./list-stories.component.scss']
})
export class ListStoriesComponent implements OnInit {
  page: number = 1;

  stories: Observable<Story[]>;

  constructor(
    private storyService: StoryService,
  ) {
  }

  ngOnInit() {
    this.stories = this.storyService.getStories(this.page);
  }

  nextPage() {
    this.stories = this.storyService.getStories(++this.page);
  }
}
