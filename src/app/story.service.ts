import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/toArray';

import { Observable, ReplaySubject } from 'rxjs/Rx';
import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';

import { Item, Story } from './story';

@Injectable()
export class StoryService {
  private topStoriesUrl = 'https://hacker-news.firebaseio.com/v0/topstories.json';

  private itemUrl(id: number): string {
    return `https://hacker-news.firebaseio.com/v0/item/${id}.json`;
  }

  private storiesStream: ReplaySubject<number> | null = new ReplaySubject(500);

  constructor(
    private http: Http,
  ) {
    this.http.get(this.topStoriesUrl)
      .mergeMap(response => response.json() as number[])
      .subscribe(id => this.storiesStream.next(id));
  }

  getStory(id: number): Observable<Story> {
    return this.http.get(this.itemUrl(id))
      .map<Response, Item>(response => response.json())
      .map<Item, Story>(({ by, descendants, id, score, time, title, url }) => ({
        id, score, time, title, url, author: by, commentCount: descendants
      }));
  }

  getStories(page: number = 1): Observable<Story[]> {
    return this.storiesStream
      .skip((page - 1) * 30)
      .take(30)
      .mergeMap(id => this.getStory(id))
      .toArray();
  }
}
